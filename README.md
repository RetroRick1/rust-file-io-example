# Description

This is a simple Rust project to test structs, implements, borrow and file I/O

## Instalation
1. Make sure to have the latest Rust version installed and the latest version of cargo
2. Clone this repo, open the terminal in the root of destination folder and type

3. 
```Bash
   cargo run
```

## License
[GNU General Public License v3.0] (https://www.gnu.org/licenses/gpl-3.0.en.html)