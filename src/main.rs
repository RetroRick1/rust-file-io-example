use std::io;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

struct Student{
    name: String,
    student_number: u8,
    tests: [f32; 3],
    oral: u8,
    final_score: f32
}

impl Student{
    fn new() -> Self{
        Student{
            name: " ".to_string(),
            student_number: 0,
            tests:  [ 0.0, 0.0, 0.0 ],
            oral: 0,
            final_score:  0.0,
        }
    }

    fn register_student(&mut self){
        println!("Please insert the student name");
        io::stdin()
        .read_line(&mut self.name)
        .expect("failed to read from stdin");

        println!("Please insert the student number");
        let mut temp  = String::new();
        io::stdin()
        .read_line(&mut temp)
        .expect("failed to read from stdin");
        let sn_trim = temp.trim();
        match sn_trim.parse::<u8>() {
            Ok(i) => self.student_number = i,
            Err(..) => println!("{} is not a valid student number", temp),
        };

        for i in 0..3{
            println!("Please insert the score of test {}", i);
            let mut temp  = String::new();
            io::stdin()
            .read_line(&mut temp)
            .expect("failed to read from stdin");
            let ts_trim = temp.trim();
            match ts_trim .parse::<f32>() {
                Ok(x) => self.tests[i] = x,
                Err(..) => println!("{} is not a test score", temp),
            };
        }

        println!("Please insert the oral score");
        let mut temp  = String::new();
        io::stdin()
        .read_line(&mut temp)
        .expect("failed to read from stdin");

        let oral_trim = temp.trim();
        match oral_trim.parse::<u8>() {
            Ok(i) => self.oral = i,
            Err(..) => println!("{} is not a valid oral score", temp),
        };
    }

    fn get_final_score(&mut self){
        self.final_score = ( 0.3 * self.tests[0] as f32 )  + ( 0.3 * self.tests[1] as f32 ) + ( 0.3 * self.tests[2] as f32 ) + (0.1 * self.oral as f32 );
    }

    fn print_student_info(&self){
        println!("Student Name: {}", self.name);
        println!("Student Number: {}", self.student_number);
        for i in 0..3{
            println!("Student Test {} Score: {}", i , self.tests[i]);
        }
        println!("Student Oral Score: {}", self.oral);
        println!("Student Final Score: {}", self.final_score);
    }

    fn create_file(&self){
        let path = Path::new("student.txt");
        let display = path.display();
    
        let mut file = match File::create(&path) {
            Err(why) => panic!("couldn't create {}: {}", display, why),
            Ok(file) => file,
        };

        match file.write_all("Name: ".as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }
        let temp = self.name.clone() + "\n";
        match file.write_all(temp.as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }
        match file.write_all("Student Number: ".as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }
        match file.write_all((self.student_number.to_string() + "\n").as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }

        for i in 0..3{
            match file.write_all("Student Test: ".as_bytes()){
                Err(why) => println!("{}", why),
                Ok(_) => println!("wrote to: {}", display)
            }
            match file.write_all((self.tests[i].to_string() + "\n").as_bytes()){
                Err(why) => println!("{}", why),
                Ok(_) => println!("wrote to: {}", display)
            }
        }

        match file.write_all("Student Oral Score: ".as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }

        match file.write_all((self.oral.to_string()+ "\n").as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }

        match file.write_all("Student Final Score: ".as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }

        match file.write_all((self.final_score.to_string() + "\n").as_bytes()){
            Err(why) => println!("{}", why),
            Ok(_) => println!("wrote to: {}", display)
        }
    }
}

fn main() {
    let mut new_student = Student::new();
    new_student.register_student();
    new_student.get_final_score();
    new_student.print_student_info();
    new_student.create_file()
}
